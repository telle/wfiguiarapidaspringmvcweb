<%@ include file="../templates/taglibs.jsp" %>

<form:form action="${pageContext.request.contextPath}/selectores-enlazados/submit" modelAttribute="direccionCuenta">
	<form:select id="comunidades" path="comunidadId" style="width: 150px">
		<form:options items="${comunidadList}" itemLabel="nombre" itemValue="id"/>
	</form:select>
	
	<spring:url var="url" value="/selectores-enlazados/provincias">
		<spring:param name="_MODIFY_HDIV_STATE_" value="provinciaId" />
		<spring:param name="id" value="hdiv-values-ref:comunidadId" />
		<%--
		<spring:param name="_HDIV_REFERENCE_VALUES_" value="comunidadId" />
		<spring:param name="_HDIV_REFERENCE_PARAM_" value="id" />
		 --%>
	</spring:url>
	<form:select id="provincias" path="provinciaId" data-ajax-url="${url}" data-linked-select="true" style="width: 150px" />
	
	<spring:url var="url" value="/selectores-enlazados/municipios">
		<spring:param name="_MODIFY_HDIV_STATE_" value="municipioId" />
		<spring:param name="provinciaId" value="hdiv-values-ref:provinciaId" />
		<%--
		<spring:param name="_HDIV_REFERENCE_VALUES_" value="provinciaId" />
		<spring:param name="_HDIV_REFERENCE_PARAM_" value="provinciaId" />
		 --%>
	</spring:url> 
	<form:select id="municipios" path="municipioId" data-ajax-url="${url}"  data-linked-select="true" 
			data-parent-select="#provincias" data-parent-param-name="provinciaId" style="width: 150px" />
	
	<form:button>Submit</form:button>
	
</form:form>