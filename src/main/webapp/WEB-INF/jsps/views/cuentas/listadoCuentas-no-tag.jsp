<%@ include file="../../templates/taglibs.jsp" %>

<c:if test="${successMessage != null}">
	<div class="successMessage">
		<spring:message code="${successMessage}"/>
	</div>
</c:if>

<c:choose>
	<c:when test="${fn:length(cuentas.content) > 0}">
	
		<!-- Hidden form -->
		<form:form id="hidden-form" action="cuentas/acciones" modelAttribute="acciones">
			<form:select path="ids" multiple="true" data-no-select2="true">
				<c:forEach var="cuenta" items="${cuentas.content}">
					<form:option value="${cuenta.idUsuario}" data-id="${cuenta.idUsuario}"/>
				</c:forEach>
			</form:select>
			<form:button id="hidden-form-button">Ejecutar acci�n</form:button>
		</form:form>
	
		<!-- COntent updated by Ajax -->
		<div id="table-container">
		<table summary='<spring:message code="listadoCuentasDec"/>.' id="tablaCuentas">
			<caption><spring:message code="listadoCuentas"/></caption>
			<thead>
				<tr>
					<th></th>
					<th><spring:message code="usuario"/></th>
					<th><spring:message code="cuenta.nombre"/></th>
					<th><spring:message code="cuenta.apellido"/></th>
					<th><spring:message code="cuenta.telefono"/></th>
					<th><spring:message code="cuenta.fcreacion"/></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="cuenta" items="${cuentas.content}">
					<tr data-user-id="${cuenta.idUsuario}">
						<td><input class="row-selector" type="checkbox"/></td>
						<td>
							<spring:url var="url" value="/cuentas/${cuenta.idUsuario}"/>
							<a href="${url}">
								<c:out value="${cuenta.idUsuario}"/>
							</a>
						</td>
						<td><c:out value="${cuenta.nombre}"/></td>
						<td><c:out value="${cuenta.apellido}"/></td>
						<td><c:out value="(+34) ${cuenta.telefono}"/></td>
						<td>
							<spring:message code="format.date.short" var="format"/>
							<fmt:formatDate value="${cuenta.fcreacion}" pattern="${format}"/>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
		<div class="paginacion">
			<c:if test="${!cuentasPaginacionHandler.primeraPagina}">
				<spring:url var="url" value="/cuentas">
					<spring:param name="page" value="${cuentasPaginacionHandler.cursorDePagina - 1}"/>
					<spring:param name="pageSize" value="10"/>
					<spring:param name="_MODIFY_HDIV_STATE_" value="" />
				</spring:url>
				<a id="prev" href="${url}">
					<spring:message code="previo"/>
				</a>
			</c:if>
			<span class="paginacionIndice">
				<c:out value="${cuentasPaginacionHandler.cursorDePagina}"/>
			</span>
			<c:if test="${!cuentasPaginacionHandler.ultimaPagina}">
				<spring:url var="url" value="/cuentas">
					<spring:param name="page" value="${cuentasPaginacionHandler.cursorDePagina + 1}"/>
					<spring:param name="pageSize" value="10"/>
					<spring:param name="_MODIFY_HDIV_STATE_" value="" />
				</spring:url>
				<a id="next" href="${url}">
					<spring:message code="siguiente"/>
				</a>
			</c:if>
		</div>
		</div>
	
	</c:when>
	<c:otherwise>
		<spring:message code="listaVacia"/>
	</c:otherwise>
</c:choose>

<h3><spring:message code="Acciones"/></h3>
<ul>
	<li>
		<spring:url var="url" value="/cuentas/pdf"/>
		<a href="${url}">
			<spring:message code="DescargaPdf"/>
		</a>
	</li>
	<li>
		<spring:url var="url" value="/api/cuentas"/>
		<a href="${url}">
			<spring:message code="DescargaXml"/>
		</a>
	</li>
</ul>

<script>
"use strict";
$( document ).ready(function() {
	init();
});
function init(){
	$("#next, #prev").click(function(e){
		e.stopPropagation();
		e.preventDefault();
		
		var url = $(this).attr("href");
		
		$.ajax(url)
		.done(function(data) {
			
			//Reemplazar tabla
			var html = $(data);
			var container = html.find("#table-container");
			$("#table-container").replaceWith(container);
			
			// Procesar form que viene del cliente
			var form = html.find("#hidden-form");
			var options = form.find("option");
			
			var select = $("#hidden-form select");
			
			// Mostrar en la tabla los options previamente seleccionados
			jQuery.each( select.find("option"), function( i, val ) {
				var option = $(val);
				var userId = option.data("id");
				if (option.is(':checked')){
					$("#table-container tr[data-user-id='"+ userId +"']").find("input[type='checkbox']").prop('checked', true);
				}
			});
			
			// Copiar los option renderizados en servidor al select del cliente
			jQuery.each( options, function( i, val ) {
				var option = $(val);
				var userId = option.data("id");
				var length = select.find("option[data-id='"+ userId +"']").length;
				if(length == 0){
					select.append(option);
				}
			});
			
			init();// Volver a asociar los listeners
		})
		.fail(function() {
			console.error("error...");
		});
	});
	
	$(".row-selector").change(function(e){
		var checkbox = $(this);
		var checked = checkbox.is(':checked');
		var userId = checkbox.closest("tr").data("user-id");
		//console.debug("User: " + userId + "Checked: " + checked);
		
		var option = $("#hidden-form option[data-id='"+ userId +"']")
		option.attr('selected', checked);
	});
}
</script>
<style>
input[type="checkbox"]{
	width: 12px;
}
#hidden-form select{
	/* */
	display: none;
}
</style>