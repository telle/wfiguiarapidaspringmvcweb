<%@ include file="../../templates/taglibs.jsp" %>
	
<div id="dyn_container">
	<zerga:table id="cuentas" baseURL="/cuentas" data="${cuentas}">
		<zerga:rowId>${row.idUsuario}</zerga:rowId>
		<zerga:columns>
		<zerga:column titleKey="usuario" property="idUsuario" />				
		<zerga:column titleKey="cuenta.nombre" property="nombre" />
		</zerga:columns>
		<zerga:actions>
			<zerga:action min="1" max="8" titleKey="accion.accion1" tooltipKey="accion.paginador" url="/cuentas/acciones/1"/>
			<zerga:action min="2" max="4" titleKey="accion.accion2" tooltipKey="accion.paginador" url="/cuentas/acciones/2"/>
			<zerga:action min="3" max="0" titleKey="accion.accion3" tooltipKey="accion.paginador" url="/cuentas/acciones/3"/>
		</zerga:actions>
	</zerga:table>
</div>
			