<%@ include file="../../templates/taglibs.jsp" %>

<form:form action="" method="POST" modelAttribute="cuentaForm">
	<ol>
		<li>
			<label for="idioma">
				<spring:message code="idioma"/><em>*</em>
			</label>
			<spring:url value="/cuentas/idiomas" var="url" />
			<form:select path="cuenta.perfil.idioma" id="idioma" cssErrorClass="error" data-ajax-url="${url}" />
		</li>
	</ol>
</form:form>