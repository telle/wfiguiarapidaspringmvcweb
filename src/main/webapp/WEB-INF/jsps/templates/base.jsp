<!doctype html>
<%@ include file="taglibs.jsp" %>
<html lang="${springRequestContext.locale}">
	<head>
		<%@ page 
		language="java"
		contentType="text/html; charset=ISO-8859-1"
		pageEncoding="ISO-8859-1"
		%>
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta charset="ISO-8859-1">
		<title><spring:message code="GuiaRapida" /></title>
		<!-- JQuery -->
		<script src='<spring:url value="/estatico/lib/jquery-1.8.1.js"/>'></script>
		<script src='<spring:url value="/estatico/lib/select2/js/select2.js"/>'></script>
		<script src='<spring:url value="/estatico/javascript/guia-rapida.js"/>'></script>
	
		<!--  Anseki overlay plugin -->
	    <script src="${pageContext.request.contextPath}/estatico/javascript/plugins/anseki/jquery.plainoverlay.min.js"><jsp:text>&amp;nbsp;</jsp:text></script>
	
		<!-- ZERGA Single Web Page Communication -->
    	<script src="${pageContext.request.contextPath}/estatico/javascript/jqrequest.js"><jsp:text>&amp;nbsp;</jsp:text></script>
		<!--  ZERGA Domain files -->
		<script src="${pageContext.request.contextPath}/estatico/javascript/domain/ZE00_core.js"><jsp:text>&amp;nbsp;</jsp:text></script>
	
		<!-- Estilos de la aplicación -->
		<!-- Bootstrap Core CSS -->
    	<link href="${pageContext.request.contextPath}/estatico/css/bootstrap.min.css" rel="stylesheet"/>
    
		<link href='<spring:url value="/estatico/css/style.css"/>' rel="stylesheet" type="text/css" />
		<link href='<spring:url value="/estatico/lib/select2/css/select2.css"/>' rel="stylesheet" type="text/css" />
	</head>
	<body>
		<div id="a11ycontrols" class="invisible">
			<ul>
			  <li><a href="#skip">Skip to Content</a></li>
			</ul>
		</div>
		<div id="menu-wrapper">
			<tiles:insertAttribute name="menu-sup"/>
		</div>
		<div id="header-wrapper">
			<tiles:insertAttribute name="cabecera"/>
		</div>
		<div id="wrapper">
			<!-- end #header -->
			<div id="page">
				<div id="page-bgtop">
					<div id="page-bgbtm">
						<tiles:insertAttribute name="menu-izq"/>
						<!-- end #sidebar -->
						<div id="content">
							<div id="skipwrapper" class="invisible"><a id="skip">-</a></div>
							<tiles:insertAttribute name="cuerpo"/>
						</div>
						<!-- end #content -->
						<div style="clear: both;">&nbsp;</div>
					</div>
				</div>
			</div>
			<!-- end #page -->
		</div>
		<tiles:insertAttribute name="pie"/>
		
	</body>
</html>