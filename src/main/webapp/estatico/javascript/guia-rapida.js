(function($) {
	"use strict";

	var applySelect2 = function() {
		var selects = $("select:not([data-select2-applied])");
		selects = selects.filter(":not([data-no-select2])");
		
		selects.each(function() {
			var $select = $(this);
			var ajaxUrl = $select.attr("data-ajax-url");
			var linkedSelect = $select.attr("data-linked-select");
			
			// Select2 default options
			var options = {
				minimumInputLength : 1,
				debug : true
			};

			if (ajaxUrl) {
				initializeSelect2Ajax(options, ajaxUrl);
			}

			if (linkedSelect) {
				initializeLinkedSelect(options, $select);
			}

			$select.select2(options);
			$select.attr("data-select2-applied", true);
		});
	};

	var initializeLinkedSelect = function(options, $select, parentSelect) {
		var parentSelect = $select.attr("data-parent-select");
		var $parentSelect = null;
		if (parentSelect) {
			$parentSelect = $(parentSelect);
		} else {
			$parentSelect = findPreviousSelect($select);
		}
		
		var parentParamName = $select.attr("data-parent-param-name");
		if (!parentParamName) {
			parentParamName = "id";
		}
		
		var dataFunction = function(params) {
			var queryParameters = {}
			queryParameters['q'] = params.term;
			queryParameters[parentParamName] = $parentSelect.val();

			return queryParameters;
		};

		$parentSelect.on("change", function() {
			var $parentSelect = $(this);
			
			$select.empty();
			
			if ($parentSelect.has('option').length == 0) {
				$select.attr("disabled", "true")
			} else {
				$select.removeAttr("disabled");
			}
			
			$select.trigger("change");
		});

		$select.attr("disabled", "true")

		options.ajax.data = dataFunction;
	};
	
	var findPreviousSelect = function($select) {
		var selectId = $select.attr("id");
		
		var $form = $select.closest("form");
		var $previousSelect = null;
		
		$form.find("select").each(function() {
			var $currentSelect = $(this);
			
			if ($previousSelect == null) {
				$previousSelect = $currentSelect;
				return true;
			}
			
			if ($currentSelect.attr("id") == selectId) {
				return false;
			} 
			
			$previousSelect = $currentSelect;
		});
		
		return $previousSelect;
	}

	var initializeSelect2Ajax = function(options, ajaxUrl) {
		// Select2 ajax default options
		options.ajax = {
			dataType : 'json',
			delay : 250,
			cache : false
		};

		options.ajax.url = ajaxUrl;
	};

	$(document).ready(function() {
		applySelect2();
	});

	$(document).ajaxComplete(function() {
		applySelect2();
	});

	$(document).on("click", "a[data-ajax-container]", function(event) {
		event.preventDefault();
		var link = event.target;
		var url = $(link).attr("href");
		var div = $(link).attr("data-ajax-container");
		$(div).load(url);
	});

})(jQuery);