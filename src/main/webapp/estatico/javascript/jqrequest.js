var CONTAINER_DIV_ID = "dyn_container";
var HASHBANG = "#!";
var CONTEXTO = "/zerga-web";
var POST_METHOD = 'POST';
var GET_METHOD = 'GET';

function formSubmit(options) {
	var formDivSelector = "#" + options.formDiv;
	var form = document.getElementById(options.formDiv);
	var formDiv = jQuery(formDivSelector);
	var action = jQuery("#" + options.formDiv).attr('action');

	options.url = action;

	// Evitar doble click.
	/*
	 * if (formDiv.preventRequest()) { return false; }
	 */

	// Por el momento las url son correctas y no se requieren cambios por las pestañas.
//	var baseUrl = getBaseUrl();
//	var url = getUrl(baseUrl, action);
//	if (options.tabs_on && 'false' != options.tabs_on && options.tabTarget) {
//		url = addParameter(url, options.tabs_on, options.tabTarget);
//	}
	//form.action = url;

	var ajaxOptions = {
		type : POST_METHOD,
		success : function(result, textStatus, request) {
			// manageSearchView(options, result);

			// Liberamos el control del doble click.
			// formDiv.releaseRequest();

			if (options.eatTarget) {
				eatDiv(options.eatTarget);
			}
			
			if (options.closeModal) {
				closeModal();
			}

			// TODO gestion de errores de validacion.
			formDiv.find(">div.errors").html('');

			/**
			 * Si no establecen target div es que queremos reemplazar la pagina
			 * entera.
			 */

			if (options.target) {
				jQuery("#" + options.target).html(result);
			}

//			if (options.tabs_on && 'false' != options.tabs_on) {
//				doTab(options.tabs_on, options.tabTarget, "#" + options.target,
//						result);
//				preventTargetTabClick(options.tabs_on, options.tabTarget);
//			}

			clearMask(formDivSelector);

			if (options.success) {
				options.success();
			}
			//ctxMenuUtils.loadCtxMenu();
		},
		error : function(request) {
			//clearMask(formDivSelector);
			//formDiv.releaseRequest();
			errorFunction(request, options);
		}
	};

	//loadMask(formDivSelector);

	formDiv.ajaxSubmit(ajaxOptions);
	//establishFormSubmitEvent(formDiv);

//	if (options.tabs_on && 'false' != options.tabs_on) {
//		preventTargetTabClick(options.tabs_on, options.tabTarget);
//	}
//	removeFormCopy(options);
	return false;

}

function establishFormSubmitEvent(formDiv) {
	var previous = formDiv.onsubmit;
	if (typeof previous === 'function') {
		console.log("form previous submit function:" + previous);
	} else {
		// Enviando con el plugin de formularios.
	    // bind to the form's submit event 
		formDiv.submit(function() { 
	        // inside event callbacks 'this' is the DOM element so we first 
	        // wrap it in a jQuery object and then invoke ajaxSubmit 
			try {
				jQuery(this).ajaxSubmit(ajaxOptions);
			} catch(e) {
				console.log(e);
			}
	 
	        // !!! Important !!! 
	        // always return false to prevent standard browser submit and page navigation 
	        return false; 
	    }); 
		
	}
		 
}

function setDiv(options) {
	// TODO Doble click.
//	if (jQuery('#' + options.target).preventRequest()) {
//		return false;
//	}

//	loadMask(options.target);
	var url = options.url;
	if (options.tabs_on && 'false' != options.tabs_on && options.tabTarget) {
		url = addParameter(url, options.tabs_on, options.tabTarget);
	}

	jQuery.ajax({
		type : GET_METHOD,
		url : url,
		success : function(result, textStatus, request) {
			clearMask();
			// TODO doble click
			// jQuery('#' + options.target).releaseRequest();

			// TODO Ultima busqueda.
			// manageSearchView(options, result);
			jQuery('#' + options.target).html(result);
			jQuery('[autofocus]:first').focus();
			if (options.tabs_on && 'false' != options.tabs_on) {
				doTab(options.tabs_on, options.tabTarget, '#' + options.target,
						result);
			}
			
			if (options.success) {
				options.success();
			}
//			if (typeof options.window == 'undefined' || !options.window) {
//				ctxMenuUtils.loadCtxMenu();
//			}
			if (options.eatTarget) {
				eatDiv(options.eatTarget);
			}
			historyPush(options, url);
		},
		error : function(request) {
			// TODO Doble click.
			//jQuery('#' + options.target).releaseRequest();
			clearMask();
			errorFunction(request, options);
		}
	});
	return false;
}

function eatDiv(target) {
    var div = '#' + target;
    if (jQuery(div).is(":visible")){
        jQuery(div).slideUp("1000", function() {
            jQuery(this)
            .hide()
            .text('')
            .show();
        });
    } else {
        jQuery(div).text('');
    }

    /*jQuery("#" + target).hide("explode", 1000);*/
    return false;
}

function closeModal() {
	jQuery('#modal-dialog').modal('hide');
	jQuery('#dyn_modal').modal('hide');
	jQuery('body').removeClass('modal-open');
	jQuery('.modal-backdrop').remove();
}

function loadMask(divId) {
	jQuery('#' + divId).plainOverlay('show', {
		duration : 10000
	});
}

function clearMask(divId) {
	jQuery('#' + divId).plainOverlay('hide');
}

errorFunction = function(request, options) {
	var s = request.status + "";
	var text = "";
	if (request.readyState == 4) {
		text = request.responseText;
	}

	var showPopup = true;
	switch (s) {
	case "400":
		var $errorDiv = jQuery("#" + options.formDiv + ">div.errors");

		if ($errorDiv.length) {
			// si ya existe un div donde poner los errores, los ponemos en �l
			$errorDiv.html(text);
		} else {
			// si no existe div donde poner los errores, lo creamos y lo metemos
			// al principio
			// del div indicado en las opciones
			var $formDiv = jQuery("#" + options.formDiv);
			$formDiv.prepend('<div class="errors"></div>');
			$errorDiv = $formDiv.children('.errors');
			$errorDiv.html(text);
		}
		showPopup = false;
		break;
	case "412":
		jQuery("#" + options.formDiv).html(text);
		showPopup = false;
		break;
	case "401":
		location.href = "/gt_web/login.jsp?message=" + text;
		break;
	case "500":
		showPopup = true;
		break;
	}
	if (showPopup) {
		popupError(text);
	}
};

function popupError(message) {
	alert(message);
	
    var div = '#dyn_modal';    
    jQuery(div).html(message);
    return false;
	// return popupDiv('/gt_web/gt2/util/messages/error.view?message=' +
	// encodeURI(message));
}

function historyPush(options, url) {
	// Los envios de formularios no los guardamos.
	if (options.type === POST_METHOD) {
		return false;
	}

	if (!options.historyEvent && options.target == CONTAINER_DIV_ID) {
		// TODO revisar funcionamiento de location.pathname y url.
		var href = location.href;
		var index = location.href.indexOf(HASHBANG);
		if (index > -1) {
			href = href.substring(0, index);
			
	    } 
		var hashedUrl = href + HASHBANG + url.substr(CONTEXTO.length);
		//location.href = hashedUrl;
		history.pushState({}, '', hashedUrl);
	}
}

jQuery(window).on("load", function() {
//	$( 'a[href="#"]' ).click( function(e) {
//	    e.preventDefault();
//	 } );
    var index = location.href.indexOf(HASHBANG);
    if (index > -1) {
    	loadHashedUrl(location.href)
    } 

});

//jQuery(window).on("click", function() {
//	$( 'a[href="#"]' ).click( function(e) {
//	    e.preventDefault();
//	 } );
//});

/**
 *  Chrome and Firefox treat that popstate event differently. 
 *  While Firefox doesn't fire it up on the first load, Chrome does
 */
jQuery(window).on("popstate", function() {
    var index = location.href.indexOf(HASHBANG);
    if (index > -1) {
    	return loadHashedUrl(location.href)
    } 
});

function loadHashedUrl(href) {
    var index = href.indexOf(HASHBANG);

    // saltamos el hash y la admiracion.
	index = index + HASHBANG.length; 
	
	var fragmentId = href.substr(index); 
	
	url = CONTEXTO + fragmentId;
	var options = {
			url: url,
			target: 'dyn_container',
			historyEvent: true
	};
	return setDiv(options);
	
}

