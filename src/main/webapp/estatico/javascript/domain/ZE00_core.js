function setDivDynContainer(id, url) {
    var options = {
        url: url,
        target: 'dyn_container'
    };
    return setDiv(options);
}

function popupDiv(id, url) {
    var options = {
        url: url,
        target: 'dyn_modal'
    };
    return setDiv(options);
}

/**
 * Funcion para obtener los elementos seleccionados del paginador con id indicado
 * Para conocer el número de elementos, usar getSelectedIds(pagId).length
 */
function getSelectedIds(paginationId) {
	return $('#' + paginationId +'_selectionForm input').map(function() {return this.value;});
}

/**
 * Metodo para actualizar el combo de seleccion de acciones  del paginador con id indicado en funcion del numero de elementos
 * seleccionados y de los atributos min y max de cada accion
 */
function updateActionSelect(paginationId) {
	var num = getSelectedIds(paginationId).length;
    $('#' + paginationId +'_actionSelection option').each(function( index ) {
      //console.log( index + ": " + $( this ).text() +  $( this ).data("min") +  $( this ).data("max"));
      if (num < $( this ).data("min") || ($( this ).data("max") != "0" && num > $( this ).data("max"))) {
          //console.log(index + " must not be shown for " + num + " selected items");
          $(this).prop('disabled', true);
      }
      else {
          $(this).prop('disabled', false);
      }
    });
    $('#' + paginationId +'_actionSelection').select2({placeholder: $("#" + paginationId + "_actionSelection").data("placeholder") });
}

function loadPagination(paginationId, url) {
    // Obtener elementos seleccionados
	var selectedIDs = $('#' + paginationId + '_selectionForm input').map(function() {return this.value;}).get();
	//console.log(selectedIDs);
	
	//Valores de los formularios de acciones
	var hiddenActionForms = $('#' + paginationId + '_action_forms_container form');
	var actionSelects = hiddenActionForms.find("select");
	
	// Cargar paginador
	var options = {
	        url: url,
	        target: 'dyn_container',
	        success: function() {
	        	// Recrear bloque elementos seleccionados
	        	jQuery.each( selectedIDs, function( i, val ) {
	        		var template = '<input type="hidden" name="selectedIDs" value="'+val+'"/>';
	        		//console.log(template);
	        		$('#' + paginationId + '_selectionForm').append(template);
	        		// Marcar el elemento si existe en la pagina actual
	        		$('.table#' + paginationId + ' input:checkbox[name="selection"][value="' + val + '"]').prop('checked', true);
	        	});
	        	
	        	//Actualizar formularios de acciones, necesario para soporte HDIV
        		var hiddenActionForms = $('#' + paginationId + '_action_forms_container form');
        		var selects = hiddenActionForms.find("select");
        		
        		jQuery.each( selects, function( i, val ) {
        			var select = $(val);
        			// Add previous values to the select
        			var previousSelect = actionSelects.get(i);
        			var previousOptions = $(previousSelect).find("option");
        			// Añadir option previas
        			jQuery.each( previousOptions, function( index, option ) {
        				// mirar si existe, si es así no añadir
        				var id = $(option).data("id");
        				var length = select.find("option[data-id='"+ id +"']").length;
        				if(length == 0){
        					select.append(option);
        				}
        			});
        			
        		});
        		
	        }
	    };
	return setDiv(options);
	
}

/**
 * Método ejecutado al seleccionar una acción de la lista de acciones 
 */
function executeAction(paginationId, actionFormId) {
	// Form que contiene los datos a mandar al servidor
	// falta por seleccionar la selección de filas del usuario.
	var form = $("#"+paginationId+"-action-form");
	var url = form.data("action-url-"+actionFormId);
	form.attr("action", url);
	
	var selectionIds = $("#"+paginationId+"_selectionForm input[type=hidden]");
	jQuery.each( selectionIds, function( i, val ) {
		var id = val.value;
		form.find("option[data-id='" + id + "']").attr('selected', "true");
	});
	//Mandar el formulario
	$.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function (data) {
            alert(data);
        },
        error: function (data) {
            alert("ERROR: " + data);
        }
    });
}

function resetChild(childId){
	$('#'+childId).val('');
}

function loadChild(parentId,childId,childUrl) {
	
	resetChild(childId);

	var value=$("#"+parentId).val();

	$("#"+childId).select2({
		dropdownParent: $('.modal'),
		minimumResultsForSearch: 2,
		containerCssClass: 'form-control',
		ajax: { 
			url:childUrl+value,
			dataType: 'json', 
			delay: 250,
			processResults: function (data, page) {return { results: data };}, 
			cache: true,	
			error: function(error) { 
				console.log('Se ha producido un error de comunicación con el servidor');
				var msg = '<label class=\"error\" for=\"users\">Se ha producido un error de comunicación con el servidor</label>';
				$('div#error').html(msg);}, 
			escapeMarkup: function (markup) { return markup; },
		}
	});
	
}

