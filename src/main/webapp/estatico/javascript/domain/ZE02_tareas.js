// TAREAS INICIO
function cargarListaTareas(url) {
    var options = {
        url: url,
        target: 'dyn_container'
    };
    return setDiv(options);
}

function cargarDetalleTarea(url) {
    var options = {
        url: url,
        target: 'dyn_container'
    };
    return setDiv(options);
}

function cargarAceptarTarea(url) {
    var options = {
        url: url,
        target: 'dyn_container'
    };
    return setDiv(options);
}


function cargarFinalizarTarea(url) {
    var options = {
        url: url,
        target: 'dyn_container'
    };
    return setDiv(options);
}

function cargarReasignarTarea(url) {
    var options = {
        url: url,
        target: 'dyn_container'
    };
    return setDiv(options);
}

function cargarBorrarTarea(url) {
    var options = {
        url: url,
        target: 'dyn_container'
    };
    return setDiv(options);
}

function cargarRechazarTarea(url) {
    var options = {
        url: url,
        target: 'dyn_container'
    };
    return setDiv(options);
}

function submitFormularioUpdateTarea() {	
	var options = {
	    	formDiv: 'form_detalle_tarea',
	    	target: 'dyn_container',
	    	closeModal: true
	    };
    return formSubmit(options);
}

function submitFormularioAceptarTarea() {	
	var options = {
	    	formDiv: 'form_aceptar_tarea',
	    	target: 'dyn_container',
	    	closeModal: true
	    };
    return formSubmit(options);
}


function submitFormularioFinalizarTarea() {	
	var options = {
	    	formDiv: 'form_finalizar_tarea',
	    	target: 'dyn_container',
	    	closeModal: true
	    };
    return formSubmit(options);
}

function submitFormularioReasignarTarea() {	
	var options = {
	    	formDiv: 'form_reasignar_tarea',
	    	target: 'dyn_container',
	    	closeModal: true
	    };
    return formSubmit(options);
}

function submitFormularioBorrarTarea() {	
	var options = {
	    	formDiv: 'form_borrar_tarea',
	    	target: 'dyn_container',
	    	closeModal: true
	    };
    return formSubmit(options);
}

function submitFormularioRechazarTarea() {	
	var options = {
	    	formDiv: 'form_rechazar_tarea',
	    	target: 'dyn_container',
	    	closeModal: true
	    };
    return formSubmit(options);
}

