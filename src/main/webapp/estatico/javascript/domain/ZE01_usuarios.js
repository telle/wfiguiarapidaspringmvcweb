// USUARIOS INICIO
function cargarPerfilUsuario(url) {
    var options = {
        url: url,
        target: 'dyn_container'
    };
    return setDiv(options);
}


function cargarListaUsuarios(id,url) {
    var options = {
        url: url,
        target: 'dyn_container'
    };
    return setDiv(options);
}

function submitFormularioAltaUsuario(url) {	
	var options = {
	    	formDiv: 'form_alta_usuario',
	    	target: 'dyn_container',
	    	closeModal: true
	    };
    return formSubmit(options);
}

function submitFormularioDeshabilitarUsuario(url) {	
	var options = {
	    	formDiv: 'form_deshabilitar_usuario',
	    	target: 'dyn_container',
	    	closeModal: true
	    };
    return formSubmit(options);
}

function submitFormularioHabilitarUsuario(url) {	
	var options = {
	    	formDiv: 'form_habilitar_usuario',
	    	target: 'dyn_container',
	    	closeModal: true
	    };
    return formSubmit(options);
}

function submitCambiarPassword() {
	var options = {
	    	formDiv: 'form_cambiar_password',
	    	eatTarget: 'dyn_perfil_usuario',
	    	success: function() {
	    		alert('todo popupdiv ok');
			}
	    };
    return formSubmit(options);
	
}
// USUARIOS FIN

// PROFILES INICIO
function submitFormularioAltaProfile(url) {	
	var options = {
	    	formDiv: 'form_alta_profile',
	    	target: 'dyn_container',
	    	closeModal: true
	    };
    return formSubmit(options);
}

function submitFormularioEditarProfile(url) {	

	var options = {
	    	formDiv: 'form_editar_profile',
	    	target: 'dyn_container',
	    	closeModal: true
	    };
    return formSubmit(options);
}

function cargarDetalleProfile(url) {
    var options = {
        url: url,
        target: 'dyn_container'
    };
    return setDiv(options);
}

function submitFormularioEliminarProfile(url) {	
	var options = {
	    	formDiv: 'form_eliminar_profile',
	    	target: 'dyn_container',
	    	closeModal: true
	    };
    return formSubmit(options);
}

function submitFormularioNuevoProfileUsuario(url) {	
	
	alert ('submitFormularioNuevoProfileUsuario');
	
	var options = {
	    	formDiv: 'form_nuevo_profile_usuario',
	    	target: 'dyn_container',
	    	closeModal: true
	    };
    return formSubmit(options);
}
// PROFILES FIN

// PERMISOS INICIO
function submitFormularioNuevoPermisoUsuario(url) {	
	var options = {
	    	formDiv: 'form_nuevo_permiso_usuario',
	    	target: 'dyn_container',
	    	closeModal: true
	    };
    return formSubmit(options);
}
//PERMISOS FIN
