package net.izfe.g240.wfiguiarapida.web;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import net.izfe.g610.swsvariablesentorno.VariablesEntorno;

/**
 * Clase de framework izfe que extiende {@link AbstractAnnotationConfigDispatcherServletInitializer} para que defina el
 * {@link Profile} a partir de la librería de {@link VariablesEntorno}.
 * <p>
 * TODO también se puede definir la configuración común para todas las aplicaciones de IZFE. Por ejemplo, filtros,
 * listeners, etc.
 * <p>
 * TODO pendiente de ser aprobado por parte de IZFE para que se mueva a la librería WFIFrameworkIZFE_webLIB.
 */
public abstract class AbstractIzfeDispatcherServletInitializer
		extends AbstractAnnotationConfigDispatcherServletInitializer {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractIzfeDispatcherServletInitializer.class);

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		super.onStartup(servletContext);

		String entorno = getEntornoFromVariablesEntorno();
		LOGGER.info("Entorno definido {}", entorno);

		servletContext.setInitParameter("spring.profiles.active", entorno);
	}

	private static String getEntornoFromVariablesEntorno() {
		String entorno = VariablesEntorno.ENTORNO_LOCAL;

		try {
			entorno = VariablesEntorno.getSwsEntorno();
		} catch (Exception e) {
			LOGGER.warn("Error leyendo variables de entorno", e);
		}

		return entorno;
	}

}
