package net.izfe.g240.wfiguiarapida.web.beans;

import java.util.List;

public class Acciones {

	private List<String> ids;

	/**
	 * @return the ids
	 */
	public List<String> getIds() {
		return ids;
	}

	/**
	 * @param ids
	 *            the ids to set
	 */
	public void setIds(List<String> ids) {
		this.ids = ids;
	}

}
