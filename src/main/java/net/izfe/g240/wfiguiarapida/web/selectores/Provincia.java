package net.izfe.g240.wfiguiarapida.web.selectores;

public class Provincia {
	private String id;

	private String nombre;

	public Provincia(String id, String nombre) {
		this.id = id;
		this.nombre = nombre;
	}

	public String getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}
}