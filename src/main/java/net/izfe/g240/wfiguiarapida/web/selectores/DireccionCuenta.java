package net.izfe.g240.wfiguiarapida.web.selectores;

public class DireccionCuenta {

	private String comunidadId;

	private String provinciaId;

	private String municipioId;

	public String getComunidadId() {
		return comunidadId;
	}

	public void setComunidadId(String comunidadId) {
		this.comunidadId = comunidadId;
	}

	public String getProvinciaId() {
		return provinciaId;
	}

	public void setProvinciaId(String provinciaId) {
		this.provinciaId = provinciaId;
	}

	public String getMunicipioId() {
		return municipioId;
	}

	public void setMunicipioId(String municipioId) {
		this.municipioId = municipioId;
	}

}