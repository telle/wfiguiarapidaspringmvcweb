package net.izfe.g240.wfiguiarapida.web.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import net.izfe.g240.wfiguiarapida.core.facades.FilenetFacade;
import net.izfe.g240.wstserviciosfilenetv2.beans.ContenidoFicheroFilenet;


@Controller
public class FileNetController {
	
	@Autowired
	private FilenetFacade filenetFacade;
	
	
	@RequestMapping("/filenet")
	public void recuperarDocumento(HttpServletResponse response) throws Exception {

		
		ContenidoFicheroFilenet filenetContenido = filenetFacade.recuperarManualFN("a");
		if(filenetContenido == null) throw new Exception ("ERROR [Content]: fichero no encontrado");
		
		escribir(response, filenetContenido.getContenido(), filenetContenido.getFileName(), "", filenetContenido.getMimeType());
		
		
/*		
		//Document document = this.getDocument("ork", "224657", null, "EN-US");
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.parseMediaType("application/x-download"));
		header.set("Content-Disposition", "attachment; filename=" + nombre);
		/header.setContentLength(contenido.length);
		httpEntity = new H
		httpEntity = new HttpEntity<byte[]>(contenido, header);
*/	}

	/**
	 * Escribe el documento en el response
	 * @param response
	 * @param doc
	 * @param titulo
	 * @param extension
	 * @param contentType
	 * @throws Exception
	 */
	public static void escribir( HttpServletResponse response, InputStream contenido,String titulo, String extension, String contentType) throws IOException{
		//response.setHeader("content-disposition","attachment; filename="+titulo.replace(' ','_')+extension);
		response.setHeader("content-disposition","filename="+titulo.replace(' ','_')+extension);
		response.setContentType(contentType);
		byte[] aux=new byte[1024];
		int count;
		OutputStream out=response.getOutputStream();
		while((count=contenido.read(aux))!=-1){
			out.write(aux, 0, count);
		}
		out.flush();
		out.close();
		contenido.close();
	}
	
}
