package net.izfe.g240.wfiguiarapida.web;

import org.springframework.context.annotation.Configuration;

@Configuration
public class GuiaRapidaSecurity {

//	/**
//	 * Configuración super básica de Spring Security. Se utiliza las pantallas que provee Spring Security para login y
//	 * logout.
//	 * <p>
//	 * La autenticación es tipo formulario y la base de datos de usuarios está en memoria. TODO cuando se decida el tipo
//	 * de autenticación que se utilizará en Zerga habrá que integrar la guía rápida con dicho sistema de autenticación.
//	 */
//	@EnableWebMvcSecurity
//	@Configuration
//	public static class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
//
//		@Autowired
//		public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//			auth.inMemoryAuthentication().withUser("user").password("password").roles("USER");
//		}
//
//		protected void configure(HttpSecurity http) throws Exception {
//			http.authorizeRequests().anyRequest().authenticated().and().formLogin();
//
//			http.csrf().disable();
//		}
//	}

	//@EnableHdivWebSecurity
//	@Configuration
//	public static class HdivConfig extends HdivWebSecurityConfigurationSupport {
//
//		@Override
//		public void configure(SecurityConfigBuilder builder) {
//			builder.sessionExpired().homePage("/").loginPage("/login");
//		}
//
//		@Override
//		public void addExclusions(ExclusionRegistry registry) {
//			 registry.addUrlExclusions("/", "/login", "/logout").method("GET");
//			 registry.addUrlExclusions("/login").method("POST");
//			 registry.addUrlExclusions("/estatico/.*");
//			 registry.addParamExclusions("_csrf");
//
//			//registry.addUrlExclusions("/.*");
//		}
//
//		@Override
//		public void configureEditableValidation(ValidationConfigurer validationConfigurer) {
//			// Aplicamos las reglas de validación de editables por defecto de hdiv
//			validationConfigurer.addValidation("/.*");
//		}
//		
//		@Override
//		public EditableDataValidationProvider editableDataValidationProvider() {
//			
//			ExtraParamEditableDataValidationProvider provider = new ExtraParamEditableDataValidationProvider();
//			provider.setValidationRepository(editableValidationRepository());
//			
//			provider.setPatternMatcherFactory(patternMatcherFactory());
//			provider.setExtraEditableParameters(extraEditableParameters());
//			return provider;
//		}
//		
//	}
}