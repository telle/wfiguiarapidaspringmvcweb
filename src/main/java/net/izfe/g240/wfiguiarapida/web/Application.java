package net.izfe.g240.wfiguiarapida.web;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
public class Application {

	@Bean(name = "messageSource")
	public MessageSource messageSource() {
		ResourceBundleMessageSource source = new ResourceBundleMessageSource();
		source.setBasenames(
				"net.izfe.hzg200.zerga.i18n.messages"
				,"net.izfe.hzg200.zerga.i18n.messages_es_ES"
				,"net.izfe.hzg200.zerga.i18n.messages_eu_ES"
				, "net.izfe.hzg200.zerga.i18n.security"
				, "net.izfe.hzg200.zerga.i18n.literales_02_presentacion_renta"
				, "net.izfe.hzg200.zerga.i18n.literales_02_admon_usuarios"
				, "net.izfe.hzg200.zerga.i18n.literales_02_tareas"
				, "net.izfe.hzg200.zerga.i18n.literales_02_comunes"
				, "net/izfe/g240/wfiguiarapida/web/resources/ApplicationResources");
		source.setUseCodeAsDefaultMessage(true);
		source.setDefaultEncoding("UTF-8");
		//comentario
		return source;
	}
	
    /**
     * Recupera el messageAccessor.
     * @return messageAccessor
     */
    @Bean(name = "messageAccessor")
    public MessageSourceAccessor messageSourceAccessor() {
    	return new MessageSourceAccessor(this.messageSource());
    }
	
}
