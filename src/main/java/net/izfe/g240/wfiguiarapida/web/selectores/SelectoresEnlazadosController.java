package net.izfe.g240.wfiguiarapida.web.selectores;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import net.izfe.g200.hzgzerga.commons.web.select.SelectOptions;

@RequestMapping("/selectores-enlazados")
@Controller
public class SelectoresEnlazadosController {

	@RequestMapping()
	public String getSelectEnlazados(Model model) {
		model.addAttribute(new DireccionCuenta());
		model.addAttribute(getComunidades());

		return "selectEnlazados";
	}

	@RequestMapping(value = "provincias", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SelectOptions getProvincias(@RequestParam String id, @RequestParam String q, Model model) {
		List<Provincia> provincias = getProvinciasList();
		return new SelectOptions(provincias, "id", "nombre");
	}

	@RequestMapping(value = "municipios", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SelectOptions getMunicipios(@RequestParam("provinciaId") String comunidadId, @RequestParam String q,
			Model model) {
		List<Municipio> municipios = getMunicipiosList();

		return new SelectOptions(municipios, "id", "nombre");
	}
	
	@RequestMapping(value = "submit", method=RequestMethod.POST)
	public String submit(@ModelAttribute DireccionCuenta direccionCuenta, BindingResult bindingResult) {

		System.out.println("ComunidadId: " + direccionCuenta.getComunidadId());
		System.out.println("ProvinciaId: " + direccionCuenta.getProvinciaId());
		System.out.println("MunicipioId: " + direccionCuenta.getMunicipioId());
		
		return "selectEnlazados";
	}

	private static List<Comunidad> getComunidades() {
		List<Comunidad> comunidades = new ArrayList<>();

		comunidades.add(new Comunidad("A", "AAAAAAAA"));
		comunidades.add(new Comunidad("B", "BBBBBBBB"));
		comunidades.add(new Comunidad("C", "CCCCCCCC"));
		comunidades.add(new Comunidad("D", "DDDDDDDD"));
		comunidades.add(new Comunidad("E", "EEEEEEEE"));

		return comunidades;
	}

	public static List<Provincia> getProvinciasList() {
		List<Provincia> provincias = new ArrayList<>();

		provincias.add(new Provincia("F", "FFFFFF"));
		provincias.add(new Provincia("G", "GGGGGG"));
		provincias.add(new Provincia("H", "HHHHHH"));
		provincias.add(new Provincia("I", "IIIIII"));
		provincias.add(new Provincia("J", "JJJJJJ"));

		return provincias;
	}

	public static List<Municipio> getMunicipiosList() {
		List<Municipio> municipios = new ArrayList<>();

		municipios.add(new Municipio("K", "KKKKKK"));
		municipios.add(new Municipio("L", "LLLLLL"));
		municipios.add(new Municipio("M", "MMMMMM"));
		municipios.add(new Municipio("N", "NNNNNN"));
		municipios.add(new Municipio("O", "OOOOOOO"));

		return municipios;
	}
}
