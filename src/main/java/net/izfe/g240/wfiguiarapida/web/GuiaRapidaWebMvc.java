package net.izfe.g240.wfiguiarapida.web;

import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesView;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

import net.izfe.g240.wfiframeworkizfelib.springmvc.excepciones.IzfeSimpleMappingExceptionResolver;

@EnableWebMvc
@ComponentScan({"net.izfe.g240.wfiguiarapida.web"/*, "net.izfe.g200.hzgzerga.commons.web.select"*/})
@Configuration
public class GuiaRapidaWebMvc extends WebMvcConfigurerAdapter {

	// @Autowired
	// @Qualifier("hdivEditableValidator")
	// private Validator hdivEditableValidator;

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/estatico/**").addResourceLocations("/estatico/");
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("inicio");
		registry.addViewController("/inicio").setViewName("inicio");
		registry.addViewController("/error").setViewName("error");
		registry.addViewController("/logout-success").setViewName("logout");
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new LocaleChangeInterceptor()).excludePathPatterns("/estatico/**");
	}

	@Bean
	public TilesViewResolver tilesViewResolver() {
		TilesViewResolver tilesViewResolver = new TilesViewResolver();
		tilesViewResolver.setViewClass(TilesView.class);
		tilesViewResolver.setRequestContextAttribute("springRequestContext");
		return tilesViewResolver;
	}

	@Bean
	public TilesConfigurer tilesConfigurer() {
		TilesConfigurer tilesConfigurer = new TilesConfigurer();
		tilesConfigurer.setDefinitions(new String[] { "/WEB-INF/tiles-defs.xml" });
		return tilesConfigurer;
	}

	@Bean
	public LocaleResolver localeResolver() {
		return new SessionLocaleResolver();
	}

	@Bean
	public HandlerExceptionResolver exceptionResolver() {
		IzfeSimpleMappingExceptionResolver exceptionResolver = new IzfeSimpleMappingExceptionResolver();
		exceptionResolver.setLogCategory("net.izfe.g240.wfiguiarapida.web");

		Properties exceptionMappings = new Properties();
		exceptionMappings.put("java.lang.Exception", "error");
		exceptionResolver.setExceptionMappings(exceptionMappings);

		return exceptionResolver;
	}

	// @Override
	// public Validator getValidator() {
	// return hdivEditableValidator;
	// }
	
}
