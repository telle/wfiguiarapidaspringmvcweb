package net.izfe.g240.wfiguiarapida.web;

import javax.servlet.Filter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.web.filter.CharacterEncodingFilter;

import net.izfe.g240.wfiframeworkizfelib.presentacion.listeners.Log4jLocalIzfeListener;
import net.izfe.g240.wfiguiarapida.core.GuiaRapidaCore;

// TODO falta configuración de Spring Security, HDIV y el filtro del Framework
public class GuiaRapidaInitializer extends AbstractIzfeDispatcherServletInitializer {

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		super.onStartup(servletContext);
		servletContext.addListener(new Log4jLocalIzfeListener());
		// servletContext.addListener(new InitListener());
	}

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { Application.class, GuiaRapidaCore.class, GuiaRapidaSecurity.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] { GuiaRapidaWebMvc.class };
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

	@Override
	protected Filter[] getServletFilters() {
		CharacterEncodingFilter encodingFilter = new CharacterEncodingFilter();
		encodingFilter.setForceEncoding(true);
		encodingFilter.setEncoding("UTF-8");
		
		// ValidatorFilter hdivFilter = new ValidatorFilter();
		
//		DelegatingFilterProxy springSecurityFilter = new DelegatingFilterProxy();
		
		return new Filter[] { encodingFilter, /* hdivFilter, springSecurityFilter */ };
	}

}
