package net.izfe.g240.wfiguiarapida.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import net.izfe.g240.wfiguiarapida.core.facades.EjemploCicsFacade;


@Controller
public class CicsController {
	
	@Autowired
	private EjemploCicsFacade ejemploCicsFacade;
	
	
	@RequestMapping("/cics")
	public String ejemploCics(Model model) {
		
		
		
		//model.addAttribute(new CuentaForm());
		String datosDevueltos = ejemploCicsFacade.obtenerImpuestos("0002950316", "INFORMA2");
		System.out.println(datosDevueltos);
		
		model.addAttribute("datosCics", datosDevueltos);
		return "cuentas/ver";
	}
}
